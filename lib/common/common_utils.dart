


import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CommonUtils {

  static showTravelBottomSheet({Widget? body, double? height, bool isScrollControlled = true, bool? drag, bool? dismissible, Color? backgroundColor}){

    showModalBottomSheet(
        context: Get.context!,
        enableDrag: drag??true,
        barrierColor: Colors.black87,
        isDismissible: dismissible??true,
        isScrollControlled: isScrollControlled,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
        ),
        builder: (BuildContext context) {
          // return your layout
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              height: height??MediaQuery.of(context).size.height -100,
              decoration: BoxDecoration(
                  color: backgroundColor??Colors.amber,
                  borderRadius: const BorderRadius.only(topRight: Radius.circular(16), topLeft: Radius.circular(16))
              ),
              child: body,
            ),
          );
        });

  }



}