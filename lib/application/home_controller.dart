
import 'package:todo_app/common/common_utils.dart';
import 'package:todo_app/common/constants.dart';
import 'package:todo_app/common/database_helper.dart';
import 'package:todo_app/domain/todo_model.dart';
import 'package:todo_app/presentation/Widgets/todo_form.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController with GetTickerProviderStateMixin {

  String? searchQuery = "";

  TabController? tabController;
  TextEditingController? titleController;
  TextEditingController? descriptionController;
  int currentIndex = 0;
  DatabaseHelper? _db;
  List<TodoModel> todoList = [];
  List<TodoModel> todoListFiltered = [];
  TodoModel? selectedNote;

  @override
  void onInit() {
    _db = DatabaseHelper();

    titleController = TextEditingController();
    descriptionController = TextEditingController();
    tabController = TabController(length: 3, vsync: this);

    tabController!.addListener(() {
      currentIndex = tabController!.index;
      update();
    });

    _recoverNotes();
    super.onInit();
  }


  noteSaveUpdate({TodoModel? note}) async {
    String title = titleController!.text;
    String description = descriptionController!.text;

    if (note == null) {
      //Saving ...
      TodoModel note = TodoModel(title, description, getTime());
      int result = await _db!.saveNote(note);
    } else {
      //Updating ...
      note.title = title;
      note.description = description;
      note.data = getTime();

      int result = await _db!.updateNote(note);
    }

    titleController!.clear();
    descriptionController!.clear();

    _recoverNotes();
  }

  removeNote(int id) async {
    await _db!.removeNote(id);
    _recoverNotes();
  }

  _recoverNotes() async {
    List notesRecovered = await _db!.recoverNote();
    List<TodoModel>? tempList = <TodoModel>[];

    for (var item in notesRecovered) {
      TodoModel note = TodoModel.fromMap(item);
      tempList.add(note);
    }

    todoList = tempList!;
    update();
    tempList = null;
  }

  setSelectedNote(TodoModel note){
    selectedNote = note;
    update();
  }

  filterNoteByDate(List<TodoModel> listN){

    switch(currentIndex){
      case 0:
        return listN.where((element) => element.data == today).toList();
        break;
      case 1:
        return listN.where((element) => element.data == tomorrow).toList();
        break;
      case 2:
        return listN.where((element) => element.data == upcoming).toList();
        break;
      default:

    }

  }

  showNoteScreen({TodoModel? note}) {
    String textUpdate = '';
    String titleUpdate = '';

    if (note == null) {
      //saving ...
      titleController!.text = '';
      descriptionController!.text = '';
      textUpdate = 'Save';
      titleUpdate = 'Add';
    } else {
      //updating ...
      titleController!.text = note.title!;
      descriptionController!.text = note.description!;
      textUpdate = 'Update';
      titleUpdate = 'Update';
    }

    CommonUtils.showTravelBottomSheet(
        dismissible: true,
        isScrollControlled: true,
        drag: false,
        height: 300,
        backgroundColor: Colors.white,
        body: TodoForm(
          title: titleUpdate,
          buttonName: textUpdate,
          onSave: (){
            if(titleController!.text.isNotEmpty) {
              noteSaveUpdate(note: note);
              Get.back();
            }else{
              Get.snackbar(
                "Error",
                "Title is Required",
                colorText: Colors.white,
                backgroundColor: Colors.red,
                icon: const Icon(Icons.error),
              );
            }

          },
        )
    );

  }

  String getTime(){

    switch(currentIndex){
      case 0:
        return today;
      case 1:
        return tomorrow;
      case 2:
        return upcoming;
      default:
        return today;
    }


  }

  searchList(String value){
    searchQuery = value;
    todoListFiltered = todoList.where((element) => element.title!.toLowerCase().contains(value.toLowerCase())).toList();

    update();
  }



}


