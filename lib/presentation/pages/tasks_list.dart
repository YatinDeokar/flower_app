


import 'package:todo_app/application/home_controller.dart';
import 'package:todo_app/domain/todo_model.dart';
import 'package:todo_app/presentation/Widgets/todo_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TasksList extends StatelessWidget {
  const TasksList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {

        List<TodoModel> list = _.filterNoteByDate(_.searchQuery!.isEmpty ? _.todoList : _.todoListFiltered);

        return Container(
          child: (list != null && list.isNotEmpty)
              ? ListView.builder(
              itemCount: list!.length,
              itemBuilder: (ctx, index){
                return TodoItem(
                  note: list[index],
                  onClick: (){},
                  onEdit: (){
                    _.showNoteScreen(note: list[index]);
                  },
                  onRemove: (){
                    _.removeNote(list[index].id!);
                  },
                );
              })
              : Center(child: Text((_.searchQuery!.isEmpty) ? "No Item found" : "Searched item Not Found", style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Colors.grey))),
        );
      }
    );
  }
}



