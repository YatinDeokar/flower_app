import 'package:todo_app/application/home_controller.dart';
import 'package:todo_app/common/common_utils.dart';
import 'package:todo_app/presentation/Widgets/todo_form.dart';
import 'package:todo_app/presentation/pages/tasks_list.dart';
import 'package:todo_app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../Widgets/search_widget.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  HomeController homeController = Get.find();

  @override
  void initState() {
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            leading: const Icon(Icons.menu),
            title: SearchWidget(),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: (){
              _.showNoteScreen();
            },
            child: const Icon(Icons.add),) ,
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [

                TabBar(
                  isScrollable: false,
                  // padding: EdgeInsets.zero,
                  // labelPadding: EdgeInsets.zero,
                  indicatorWeight: 4,
                  indicatorSize: TabBarIndicatorSize.tab,
                  controller: _.tabController,
                  labelColor: Colors.blue,
                  // indicatorColor: kPrimaryColor,
                  tabs: const [
                    Tab(text: "Today",),
                    Tab(text: "Tomorrow",),
                    Tab(text: "Upcoming",),
                  ],
                ),

                Flexible(
                  child: TabBarView(
                    controller: _.tabController,
                    children: const [
                      TasksList(),
                      TasksList(),
                      TasksList(),
                    ],
                  ),
                ),

              ],
            ),
          ),
        );
      }
    );
  }

}
