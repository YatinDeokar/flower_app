
import 'package:todo_app/application/home_binding.dart';
import 'package:todo_app/presentation/pages/home_page.dart';
import 'package:todo_app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  //hi push
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      getPages: AppPages.routes,
      initialRoute: Routes.HOME_PAGE,
      initialBinding: HomeBinding(),
      defaultTransition: Transition.fade,
      home: const MyHomePage(),
    );
  }
}







