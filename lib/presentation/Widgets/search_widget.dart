import 'package:todo_app/application/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchWidget extends StatelessWidget {
  SearchWidget({Key? key}) : super(key: key);

  final HomeController homeController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      width: 220,
      child: TextField(
        autofocus: true,
        decoration: InputDecoration(
            prefixIcon: const Icon(Icons.search),
            contentPadding: const EdgeInsets.only(top: 10, left: 12, right: 12),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            filled: true,
            hintText: 'Search...',
            hintStyle: const TextStyle(color: Colors.black),
            fillColor: Colors.white70),
        style: const TextStyle(color: Colors.black, fontSize: 16.0),
        onChanged: (val) {
          homeController.searchList(val);
        },
      ),
    );
  }
}
