import 'package:todo_app/application/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TodoForm extends StatelessWidget {
  const TodoForm({Key? key, this.title, this.buttonName, this.onSave}) : super(key: key);

  final String? title;
  final String? buttonName;
  final Function? onSave;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        return Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("$title", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
              const SizedBox(
                height: 16,
              ),
              TextFormField(
                controller: _.titleController,
                decoration: InputDecoration(
                  labelText: "Title",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.blue, width: 1.0),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 16,
              ),
              TextFormField(
                controller: _.descriptionController,
                decoration: InputDecoration(
                  labelText: "Description",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.blue, width: 1.0),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 24,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(onPressed: () {
                    Get.back();
                  }, child: const Text("Cancel")),
                  const SizedBox(width: 16,),
                  ElevatedButton(onPressed: () {
                    onSave!();
                  }, child: Text("$buttonName")),
                ],
              ),
            ],
          ),
        );
      }
    );
  }
}
