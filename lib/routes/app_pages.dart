import 'package:todo_app/application/home_binding.dart';
import 'package:todo_app/presentation/pages/home_page.dart';
import 'package:get/get.dart';

part 'app_routes.dart';

class AppPages {
  const AppPages._();

  static final routes = [

    GetPage(
      name: _Paths.HOME_PAGE,
      page: () =>  const MyHomePage(),
      binding: HomeBinding(),
    ),

  ];
}
